create table ides
(
    ide_id int auto_increment
        primary key,
    name   varchar(40) not null,
    constraint ides_name_uindex
        unique (name)
);

create table roles
(
    role_id int auto_increment
        primary key,
    type    varchar(30) not null,
    constraint roles_type_uindex
        unique (type)
);

create table users
(
    user_id      int auto_increment
        primary key,
    username     varchar(20)          not null,
    password     varchar(30)          not null,
    email        varchar(100)         not null,
    phone_number varchar(10)          not null,
    photo        varchar(500)         null,
    role_id      int                  not null,
    active       tinyint(1) default 1 not null,
    constraint users_email_uindex
        unique (email),
    constraint users_phone_number_uindex
        unique (phone_number),
    constraint users_username_uindex
        unique (username),
    constraint users_roles_fk
        foreign key (role_id) references roles (role_id)
);

create table addons
(
    addon_id      int auto_increment
        primary key,
    name          varchar(30)                                                not null,
    description   text                                                       null,
    origin        varchar(500)                                               not null,
    content       longblob                                                   not null,
    target_ide    int                                                        not null,
    avatar        varchar(500)                                               null,
    creator       int                                                        not null,
    status        enum ('pending', 'approved', 'declined') default 'pending' not null,
    downloads     int                                                        not null,
    open_issues   int                                                        not null,
    pull_requests int                                                        not null,
    last_commit   date                                                       not null,
    constraint addons_name_uindex
        unique (name),
    constraint addons_ides_fk
        foreign key (target_ide) references ides (ide_id),
    constraint addons_users_fk
        foreign key (creator) references users (user_id)
);

create table ratings
(
    rating_id int auto_increment
        primary key,
    user_id   int not null,
    addon_id  int not null,
    rating    int not null,
    constraint ratings_addons_fk
        foreign key (addon_id) references addons (addon_id),
    constraint ratings_users_fk
        foreign key (user_id) references users (user_id)
);

create table users_addons
(
    user_addon_id int auto_increment
        primary key,
    user_id       int not null,
    addon_id      int not null,
    constraint users_addons_addons_fk
        foreign key (addon_id) references addons (addon_id),
    constraint users_addons_users_fk
        foreign key (user_id) references users (user_id)
);


