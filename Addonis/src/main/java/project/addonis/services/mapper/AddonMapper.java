package project.addonis.services.mapper;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import project.addonis.exceptions.UnauthorizedOperationException;
import project.addonis.models.*;
import project.addonis.models.dtos.EditAddonDto;
import project.addonis.repositories.interfaces.AddonRepository;
import project.addonis.repositories.interfaces.IdeRepository;
import project.addonis.repositories.interfaces.TagRepository;
import project.addonis.repositories.interfaces.UserRepository;
import project.addonis.services.interfaces.AddonService;
import project.addonis.services.interfaces.UserService;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class AddonMapper {
    private static final String UNAUTHORIZED_ACCESS = "Only admin users have access to this information";
    private static final String UNAUTHORIZED_ACTION = "Only admin users are allowed to perform this action";

    private final AddonService service;
    private final AddonRepository repository;
    private final IdeRepository ideRepository;
    private final RestTemplate restTemplate;
    private final TagRepository tagRepository;
    private final Cloudinary cloudinary;



    @Autowired
    public AddonMapper(AddonService service, AddonRepository repository, IdeRepository ideRepository, RestTemplate restTemplate, TagRepository tagRepository, Cloudinary cloudinary) {
        this.service = service;
        this.repository = repository;
        this.ideRepository = ideRepository;

        this.restTemplate = restTemplate;
        this.tagRepository = tagRepository;
        this.cloudinary = cloudinary;


    }

    //create
    public Addon fromDto(AddonDto addonDto, User user) throws Exception {
        if (!user.isActiveStatus()) {
            throw new UnauthorizedOperationException("User is blocked");
        }
        String urlMain = new StringBuilder(addonDto.getLinkToGitHub()).insert(8, "api.").insert(23, "repos/").toString();
        Addon addon = new Addon();
        addon = service.updateGit(urlMain, addon);

        addon = setUpContent(addonDto.getName(), addonDto.getContent(), addon);

        addon = setUpIcon(addonDto.getName(), addonDto.getIcon(), addon);

        addon.setCreator(user);
        addon.setOrigin(addonDto.getLinkToGitHub());
        addon.setName(addonDto.getName());
        addon.setIde(ideRepository.getById(addonDto.getIde_id()));

        addon.setDescription(addonDto.getDescription());
        addon.setUploadDate(LocalDate.now());
        addon.setDownloads(0);
        addon.setStatus("pending");


        Map<Integer, Tag> all = tagRepository.getAll().stream().collect(Collectors.toMap(Tag::getId, tag -> tag));

        Set<Tag> tagSet1 = addonDto.getTagSet().stream().map(all::get).filter(Objects::nonNull).collect(Collectors.toSet());

        addon.setTagSet(tagSet1);



        return addon;
    }

    public EditAddonDto toDto(Addon addon, User user) {
        if (!user.getRole().equals("admin")) {
            if (!addon.getCreator().equals(user) && !user.isActiveStatus())
                throw new UnauthorizedOperationException(UNAUTHORIZED_ACCESS);
        }

        EditAddonDto addonDto = new EditAddonDto();
        addonDto.setDescription(addon.getDescription());
        addonDto.setTagSet(addon.getTagSet().stream().map(Tag::getId).collect(Collectors.toSet()));

        return addonDto;

    }

    //update
    public Addon fromDto(int id, Optional<String> description,
                         Optional<MultipartFile> icon, Optional<Set<Integer>> tagSet, User user) throws Exception {
        Addon addon = repository.getById(id);
        if (!user.getRole().equals("admin")) {
            if (!addon.getCreator().equals(user) || !user.isActiveStatus())
                throw new UnauthorizedOperationException(UNAUTHORIZED_ACCESS);
        }
        description.ifPresent(addon::setDescription);

        if (tagSet.isPresent()) {
            Map<Integer, Tag> all = tagRepository.getAll().stream().collect(Collectors.toMap(Tag::getId, tag -> tag));

            Set<Tag> tagSet1 = tagSet.get().stream().map(all::get).filter(Objects::nonNull).collect(Collectors.toSet());

            addon.setTagSet(tagSet1);
        }

        if (icon.isPresent()) {
            addon = setUpIcon(addon.getName(), icon.get(), addon);
        }


        return addon;
    }


    public Addon setUpContent(String name, MultipartFile content, Addon addon) throws IOException {



        //upload new photo to Cloudinary
        Map result = cloudinary.uploader().upload(content.getBytes(), ObjectUtils.asMap("resource_type", "raw", "public_id", name + ".zip"));
        //set as profile picture for user
        String linkToFile = result.get("url").toString();
        addon.setContent(linkToFile);

        //update user

        return addon;
    }

    public Addon setUpIcon(String name, MultipartFile icon, Addon addon) throws IOException {
        Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                "cloud_name", "dbnpdkoj9",
                "api_key", "123412517634854",
                "api_secret", "JFXuspTEoDwobgGMXECyqxhLz80"));

        Map cloudinaryUpload = cloudinary.uploader().upload(icon.getBytes(), ObjectUtils.asMap("public_id", name));
        String linkToIcon = cloudinaryUpload.get("url").toString();
        addon.setIcon(linkToIcon);

        return addon;
    }
}
