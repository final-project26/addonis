package project.addonis.services.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.addonis.controllers.AuthenticationHelper;
import project.addonis.exceptions.UnauthorizedOperationException;
import project.addonis.models.Role;
import project.addonis.models.User;
import project.addonis.models.dtos.UserDto;
import project.addonis.models.dtos.UsersUpdate;
import project.addonis.models.dtos.UsersUpdate;
import project.addonis.repositories.interfaces.RoleRepository;
import project.addonis.repositories.interfaces.UserRepository;

import java.io.IOException;

import project.addonis.services.interfaces.UserService;

import java.io.IOException;

@Component
public class UserMapper {

    private static final String DEFAULT_AVATAR = "https://res.cloudinary.com/dbnpdkoj9/image/upload/v1637928111/default_avatar.png";

    private static final String UNAUTHORIZED_ACTION = "Only admin users are allowed to perform this action";

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;

    @Autowired
    public UserMapper(UserRepository userRepository, RoleRepository roleRepository, AuthenticationHelper authenticationHelper, UserService userService) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
    }

    //create
    public User fromDtoToObject(UserDto userDto) {
        User user = new User();
        //once created it cannot be changed
        user.setUsername(userDto.getUsername());

        dtoToObject(userDto, user);

        Role role = roleRepository.getByField("type", "user");
        user.setRole(role);

        user.setActiveStatus(true);

        //set photo link to default avatar from Cloudinary
        user.setPhoto(DEFAULT_AVATAR);

        return user;

    }

    //-> zashto se povtarqt
    public User fromDtoToObject(int id, UserDto userDto, User user) throws IOException {
        if (!user.getRole().equals("admin") && user.getId() != id) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION);
        }

        User toUpdate = userRepository.getById(id);
        toUpdate.setEmail(userDto.getEmail());
        userService.uploadPhoto(id, userDto.getPhoto(), toUpdate);
        toUpdate.setPhoneNumber(userDto.getPhoneNumber());


        return toUpdate;
    }

    //update
    public User fromDtoToObject(int id, UsersUpdate userDto, User user) throws IOException {
        if (!user.getRole().equals("admin") && user.getId() != id) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION);
        }


        User toUpdate = userRepository.getById(id);
        toUpdate.setEmail(userDto.getEmail());
        if (!userDto.getPhoto().get().isEmpty()) {
            System.out.println("file name::!: "+userDto.getPhoto().get().getName());
            toUpdate.setPhoto(userService.uploadPhoto(id, userDto.getPhoto().get(), toUpdate));
        }
        toUpdate.setPhoneNumber(userDto.getPhoneNumber());


        return toUpdate;
    }

    private void dtoToObject(UserDto userDto, User userToUpdate) {
        userToUpdate.setEmail(userDto.getEmail() == null ? userToUpdate.getEmail() : userDto.getEmail());
        userToUpdate.setPassword(userDto.getPassword() == null ? userToUpdate.getPassword() : userDto.getPassword());
        userToUpdate.setPhoneNumber(userDto.getPhoneNumber() == null ? userToUpdate.getPhoneNumber() : userDto.getPhoneNumber());
    }

    public UsersUpdate toDto(User user, User userForAuthorization) {
        if (!userForAuthorization.getRole().equals("admin") && userForAuthorization.getId() != user.getId()) {
            throw new UnauthorizedOperationException("User is unauthorized");
        }

        UsersUpdate userDto = new UsersUpdate();
        userDto.setEmail(user.getEmail());
        userDto.setPhoneNumber(user.getPhoneNumber());
        //photo??
        userDto.setPhotoLink(user.getPhoto());

        return userDto;

    }


}
