package project.addonis.services;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import project.addonis.exceptions.DuplicateEntityException;
import project.addonis.exceptions.UnauthorizedOperationException;
import project.addonis.models.Addon;
import project.addonis.models.Rating;
import project.addonis.models.User;
import project.addonis.models.filterObjects.Ufp;
import project.addonis.repositories.interfaces.AddonRepository;
import project.addonis.repositories.interfaces.RatingsRepository;
import project.addonis.repositories.interfaces.UserRepository;
import project.addonis.services.interfaces.UserService;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.String.format;

@Service
public class UserServiceImpl implements UserService {

    private final static String INVALID_PASSWORD = "Password must be at least 8 symbols and should contain capital letter, digit and special symbol";
    private final static String INVALID_EMAIL = "Invalid email address";
    private final static String INVALID_PHONE_NUMBER = "Phone number must begin with 0 and contain 9 additional digits";

    private final static String DUPLICATE_USER_DATA = "User with this username, email or phone number already exists in the system";


    private static final String UNAUTHORIZED_ACCESS = "Only admin users have access to this information";
    private static final String UNAUTHORIZED_ACTION = "Only admin users are allowed to perform this action";

    private static final String UNSUPPORTED_DELETION = "User has active addons in the system and cannot be deleted";

    private static final String ADDON_APPROVED = "Addon %s was successfully approved and will now be available for all users";


    private final UserRepository userRepository;
    private final RatingsRepository ratingsRepository;
    private final AddonRepository addonRepository;
    private final Cloudinary cloudinary;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RatingsRepository ratingsRepository, AddonRepository addonRepository, Cloudinary cloudinary) {
        this.userRepository = userRepository;
        this.ratingsRepository = ratingsRepository;
        this.addonRepository = addonRepository;
        this.cloudinary = cloudinary;
    }

    @Override
    public List<User> getAll(User user) {
        throwIfUserIsNotAdmin(user);
        return userRepository.getAll();
    }

    @Override
    public User getById(User user, int id) {
        //change
        if(user.getId()!=id &&!user.getRole().equals("admin"))
            throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION);
        //throwIfUserIsNotAdmin(user);
        return userRepository.getById(id);
    }

    @Override
    public List<User> filter(User user, Ufp ufp) {
        throwIfUserIsNotAdmin(user);
        return userRepository.filter(ufp);
    }

    @Override
    public void create(User user) {
        validateUser(user);
        checkForDuplicity(user);
        userRepository.create(user);
    }

    @Override
    public void update(User user, User toUpdate) {
        if (!user.getRole().equals("admin") && !user.getUsername().equals(toUpdate.getUsername())) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION);
        }

        validateUser(toUpdate);
        checkForDuplicity(toUpdate);
        userRepository.update(toUpdate);
    }

    @Override
    public void blockUser(User user, int id) {
        throwIfUserIsNotAdmin(user);

        User toBlock = userRepository.getById(id);
        toBlock.setActiveStatus(false);

        userRepository.update(toBlock);
    }

    @Override
    public void unblockUser(User user, int id) {
        throwIfUserIsNotAdmin(user);

        User toUnblock = userRepository.getById(id);
        toUnblock.setActiveStatus(true);

        userRepository.update(toUnblock);
    }

    @Override
    public String approveAddon(User user, int addonId) {
        throwIfUserIsNotAdmin(user);
        Addon addon = addonRepository.getById(addonId);
        addon.setStatus("approved");
        addonRepository.update(addon);
        return format(ADDON_APPROVED, addon.getName());
    }

    @Override
    public String uploadPhoto(int id, MultipartFile photo, User user) throws IOException {
        if (!user.getRole().equals("admin") && user.getId() != id) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION);
        }

        User toUpdate = userRepository.getById(id);
        //upload new photo to Cloudinary
        Map result = cloudinary.uploader().upload(photo.getBytes(), ObjectUtils.asMap("resource_type", "raw", "public_id", toUpdate.getUsername() + ".jpg"));
        //set as profile picture for user
        String linkToPhoto = result.get("url").toString();
        System.out.println(linkToPhoto);
        toUpdate.setPhoto(linkToPhoto);

        //update user
        userRepository.update(toUpdate);
        System.out.println("photo: "+toUpdate.getPhoto());

        return linkToPhoto;
    }

    @Override
    public String rate(User user, int id, int value) {
        Rating rating = new Rating();
        Addon addon = addonRepository.getById(id);
        rating.setUser(user);
        rating.setAddon(addon);
        rating.setRating(value);

        ratingsRepository.create(rating);
        return addon.getName();
    }

    public List<User> filter(User user, Optional<String> usernameEmail, Optional<String> phoneNumber, Optional<String> sortParam){
        if(!user.getRole().equals("admin")){
            throw new UnauthorizedOperationException("Unauthorized");
        }
        return userRepository.filter(usernameEmail,phoneNumber, sortParam);
    }
    @Override
    public User getByUsername(String email) {
        return userRepository.getByUsername(email);
    }

    @Override
    public void delete(User user, int id) {
        User toDelete = userRepository.getById(id);
        //only allow deletion if User has not created any addons
        if (!toDelete.getAddons().isEmpty()) {
            throw new UnsupportedOperationException(UNSUPPORTED_DELETION);
        }
        //if User does not have addons created only admin can remove from the system
        throwIfUserIsNotAdmin(user);
        userRepository.delete(id);
    }

    private void validateUser(User user) {
        //validate fields
        validatePassword(user.getPassword());
        validateEmail(user.getEmail());
        validatePhoneNumber(user.getPhoneNumber());
    }

    private void checkForDuplicity(User user) {
        // check whether method is called from create or update
        String methodCaller = Thread.currentThread().getStackTrace()[2].getMethodName();
        boolean containsDuplicateUsername = false;
        int id;
        //username cannot be change once created
        if (methodCaller.equals("create")) {
            id = -1;
            containsDuplicateUsername = userRepository.isDuplicate("username", user.getUsername(), id);
        } else {
            id = user.getId();
        }
        boolean containsDuplicatePhoneNumber = userRepository.isDuplicate("phoneNumber", user.getPhoneNumber(), id);
        boolean containsDuplicateEmail = userRepository.isDuplicate("email", user.getEmail(), id);

        if (containsDuplicateUsername || containsDuplicatePhoneNumber || containsDuplicateEmail) {
            throw new DuplicateEntityException(DUPLICATE_USER_DATA);
        }
    }

    private void validatePassword(String password) {
        String regex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[-+_!@#$%^&*., ?]).+$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(password);

        if (!m.matches() || password.length() < 8) {
            throw new IllegalArgumentException(INVALID_PASSWORD);
        }
    }

    //Taken from OWASP Validation Regex Repository for validating email-addresses
    private void validateEmail(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-.]+([a-zA-Z0-9_+&*-.])*@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        Pattern p = Pattern.compile(emailRegex);
        Matcher m = p.matcher(email);

        if (!m.matches()) {
            throw new IllegalArgumentException(INVALID_EMAIL);
        }
    }

    private void validatePhoneNumber(String phoneNumber) {
        String phoneRegex = "^0\\d{9}$";
        Pattern p = Pattern.compile(phoneRegex);
        Matcher m = p.matcher(phoneNumber);

        if (!m.matches()) {
            throw new IllegalArgumentException(INVALID_PHONE_NUMBER);
        }
    }

    private void throwIfUserIsNotAdmin(User user) {
        if (!user.getRole().equals("admin")) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_ACTION);
        }
    }
}
