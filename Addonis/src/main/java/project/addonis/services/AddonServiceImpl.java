package project.addonis.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import project.addonis.models.Addon;
import project.addonis.models.User;
import project.addonis.repositories.interfaces.AddonRepository;
import project.addonis.services.interfaces.AddonService;

import javax.management.Query;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AddonServiceImpl implements AddonService {
    private static final String UNAUTHORIZED_ACCESS = "Only admin users have access to this information";
    private static final String UNAUTHORIZED_ACTION = "Only admin users are allowed to perform this action";

    private final AddonRepository repository;
    private final RestTemplate restTemplate;

    @Autowired
    public AddonServiceImpl(AddonRepository repository, RestTemplate restTemplate) {
        this.repository = repository;
        this.restTemplate = restTemplate;
    }

    @Override
    public List<Addon> getAll() {
        return repository.getAll().stream().filter(a -> a.getStatus().equals("approved")).collect(Collectors.toList());
    }

    @Override
    public Addon getById(int id) throws IOException {
        Addon addon = repository.getById(id);
        if (!addon.getUpdateDate().equals(LocalDate.now())) {
            String urlMain = new StringBuilder(addon.getOrigin()).insert(8, "api.").insert(23, "repos/").toString();
            addon = updateGit(urlMain, addon);
        }
        return addon;
    }

    @Override
    public void create(Addon addon) {
        repository.create(addon);


    }

    @Override
    public void update(Addon addon) {
        repository.update(addon);
    }

    @Override
    public void delete(int id, User user) {
        if (!user.getRole().equals("admin")) {
            throw new UnsupportedOperationException(UNAUTHORIZED_ACTION);
        }
        repository.delete(id);
    }

    @Override
    public void approve(int id, User user) {
        if (!user.getRole().equals("admin")) {
            throw new UnsupportedOperationException(UNAUTHORIZED_ACTION);
        }

        repository.approve(id);
    }

    @Override
    public List<Addon> filter(Optional<String> addonName, Optional<String> ideName) {
        return repository.filter(addonName, ideName);
    }

    @Override
    public List<Addon> sort(String criteria) {
        return repository.sort(criteria);
    }

    @Override
    public List<Addon> getPending() {
        return repository.getAll().stream().filter(a -> a.getStatus().equals("pending")).collect(Collectors.toList());
    }

    @Override
    public List<Addon> getPendingAddon(int id) {
        return getPending().stream().filter(a -> a.getId() == id).collect(Collectors.toList());
    }

    @Override
    public String getRating(int id) {
        return repository.calculateRating(id);
    }

    @Override
    public Addon updateGit(String urlMain, Addon addon) throws IOException {

        Map resultOpenIssuesCount = restTemplate.getForObject(urlMain, Map.class);
        addon.setOpenIssues(Integer.parseInt(resultOpenIssuesCount.get("open_issues_count").toString()));
        //  /pulls?state=closed
        String urlPullRequests = new StringBuilder(urlMain).append("/pulls?state=closed").toString();
        List resultPullRequestsCount = restTemplate.getForObject(urlPullRequests, List.class);
        addon.setPullRequests(resultPullRequestsCount.size());

        //  /commits?per_page=1
        String urlLastCommit = new StringBuilder(urlMain).append("/commits?per_page=1").toString();
        URL url = new URL(urlLastCommit);
        ObjectMapper mapper = new ObjectMapper();
        Object[] result = mapper.readValue(url, Object[].class);
        JSONArray json = new JSONArray(result);
        JSONObject lastCommit = json.getJSONObject(0);
        JSONObject commitInfo = lastCommit.getJSONObject("commit");
        JSONObject author = commitInfo.getJSONObject("author");
        ZonedDateTime format = ZonedDateTime.parse(author.get("date").toString());
        // DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

        //String toParse = format.format(DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss"));


        LocalDateTime date = format.toLocalDateTime();
        addon.setLastCommit(date);
        addon.setUpdateDate(LocalDate.now());
        return addon;
    }

    @Override
    public List<Addon> getNewest(int numOfElements) {
        if (numOfElements > getAll().size()) throw new IllegalArgumentException();
        return getAll().stream().sorted(Comparator.comparing(Addon::getUploadDate).reversed()).limit(numOfElements).collect(Collectors.toList());
    }

    @Override
    public List<Addon> getMostPopular(int numOfElements) {
        if (numOfElements > getAll().size()) throw new IllegalArgumentException();
        return getAll().stream().sorted(Comparator.comparing(Addon::getDownloads).reversed()).limit(numOfElements).collect(Collectors.toList());
    }

    @Override
    public List<Addon> getFeatured(int numOfElements) {
        if (numOfElements > getAll().size()) throw new IllegalArgumentException();
        List<Addon> copy = new ArrayList<>(getAll());
        List<Addon> toReturn = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < numOfElements; i++) {
            int randomIndex = random.nextInt(copy.size());
            toReturn.add(copy.get(randomIndex));
            copy.remove(randomIndex);

        }
        return toReturn;
    }

    @Override
    public ResponseEntity download(Addon addon) throws MalformedURLException, URISyntaxException {
        URL url = new URL(addon.getContent());
        URI uri = url.toURI();

        Resource resource = null;
        try {
            resource = new UrlResource(uri);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        int downloads=addon.getDownloads();
        addon.setDownloads(++downloads);
        repository.update(addon);
        return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/zip"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""
                        + resource.getFilename() + "\"").body(resource);
    }
}
