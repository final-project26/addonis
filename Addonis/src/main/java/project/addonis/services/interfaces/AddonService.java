package project.addonis.services.interfaces;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import project.addonis.models.Addon;
import project.addonis.models.User;
import project.addonis.repositories.interfaces.AddonRepository;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


public interface AddonService {


    List<Addon> getAll();

    Addon getById(int id) throws IOException;

    void create(Addon addon);

    void update(Addon addon);

    void delete(int id, User user);

    void approve(int id, User user);

    List<Addon> filter(Optional<String> addonName, Optional<String> ideName);

    List<Addon> sort(String criteria);

    List<Addon> getPending();

    List<Addon> getPendingAddon(int id);

    Addon updateGit(String urlMain, Addon addon) throws IOException;

    List<Addon> getNewest(int numOfElements);

    List<Addon> getMostPopular(int numOfElements);

    List<Addon> getFeatured(int numOfElements);

    String getRating(int id);

    ResponseEntity download(Addon addon) throws MalformedURLException, URISyntaxException;

}
