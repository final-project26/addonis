package project.addonis.services.interfaces;

import org.hibernate.Session;
import org.springframework.web.multipart.MultipartFile;
import project.addonis.models.User;
import project.addonis.models.filterObjects.Ufp;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> getAll(User user);

    User getById(User user, int id);

    void create(User user);

    void update(User user, User toUpdate);

    void delete(User user, int id);

    String uploadPhoto(int id, MultipartFile photo, User user) throws IOException;

    void blockUser(User user, int id);

    void unblockUser(User user, int id);

    String rate(User user, int id, int value);

    User getByUsername(String email);

    List<User> filter(User user, Optional<String> usernameEmail, Optional<String> phoneNumber, Optional<String> sortParam);

    List<User> filter(User user, Ufp ufp);

    String approveAddon(User user, int addonId);

}
