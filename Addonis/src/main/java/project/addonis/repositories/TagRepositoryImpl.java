package project.addonis.repositories;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import project.addonis.models.Tag;
import project.addonis.repositories.interfaces.TagRepository;

@Repository
public class TagRepositoryImpl extends AbstractReadRepository<Tag> implements TagRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public TagRepositoryImpl( SessionFactory sessionFactory) {
        super(Tag.class, sessionFactory);
        this.sessionFactory=sessionFactory;
    }
}
