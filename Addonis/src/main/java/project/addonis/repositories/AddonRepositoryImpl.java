package project.addonis.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import project.addonis.exceptions.EntityNotFoundException;
import project.addonis.models.Addon;
import project.addonis.models.Rating;
import project.addonis.models.User;
import project.addonis.repositories.interfaces.AddonRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;

@Repository
public class AddonRepositoryImpl extends AbstractCUDRepository<Addon> implements AddonRepository {
    public static final String NO_RATINGS_AVAILABLE = "Addon with id %d currently has no ratings in the system";

    private final SessionFactory sessionFactory;

    @Autowired
    public AddonRepositoryImpl(SessionFactory sessionFactory) {
        super(Addon.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    //
    @Override
    public List<Addon> filter(Optional<String> addonName, Optional<String> ideName) {
        try (Session session = sessionFactory.openSession()) {

            StringBuilder queryString = new StringBuilder(" from Addon ");
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();

            if (addonName.isPresent()) {
                filters.add(" name like :name");
                params.put("name", "%" + addonName.get() + "%");

            }
            if (ideName.isPresent()) {
                filters.add(" ide.name like :ideName");
                params.put("ideName", "%" + ideName.get() + "%");
            }
            if (!filters.isEmpty()) {
                queryString.append(" where ").append(String.join(" or ", filters));
            }


            Query<Addon> query = session.createQuery(queryString.toString(), Addon.class);
            query.setProperties(params);
            return query.list();
        }
    }

    @Override
    public List<Addon> sort(String criteria) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" from Addon where status = :status ");
            if (criteria != null && !criteria.equals("none")) {
                queryString.append("order by ").append(generateSortCriteria(criteria));
            } else {
                return getAll();
            }

            Query<Addon> query = session.createQuery(queryString.toString(), Addon.class);
            query.setParameter("status", "approved");
            return query.list();

        }
    }

    public String generateSortCriteria(String criteria) {
        switch (criteria) {
            case "Name, Ascending":
                return "name asc";
            case "Name, Descending":
                return "name desc";
            case "Downloads, Ascending":
                return "downloads asc";
            case "Downloads, Descending":
                return "downloads desc";
            case "Upload Date, Ascending":
                return "uploadDate asc";
            case "Upload Date, Descending":
                return "uploadDate desc";
            case "Last Commit, Ascending":
                return "lastCommit asc";
            case "Last Commit, Descending":
                return "lastCommit desc";
        }
        throw new IllegalArgumentException();
    }

    @Override
    public String calculateRating(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Rating> query = session.createQuery("from Rating where addon.id = :id", Rating.class);
            query.setParameter("id", id);

            if (query.list().size() == 0) {
                return format(NO_RATINGS_AVAILABLE, id);
            }

            double averageRating = query.list().stream().map(Rating::getRating).mapToInt(i -> i).average().getAsDouble();

            return format("Rating: %.2f", averageRating);
        }
    }

    @Override
    public void approve(int id) {
        Addon addon = getById(id);

        addon.setStatus("approved");
    }
}
