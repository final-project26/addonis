package project.addonis.repositories;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import project.addonis.models.Rating;
import project.addonis.repositories.interfaces.RatingsRepository;

@Repository
public class RatingsRepositoryImpl extends AbstractCUDRepository<Rating> implements RatingsRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public RatingsRepositoryImpl(SessionFactory sessionFactory) {
        super(Rating.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
