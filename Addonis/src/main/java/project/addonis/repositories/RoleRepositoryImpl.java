package project.addonis.repositories;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import project.addonis.models.Role;
import project.addonis.repositories.interfaces.RoleRepository;

@Repository
public class RoleRepositoryImpl extends AbstractReadRepository<Role> implements RoleRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        super(Role.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
