package project.addonis.repositories;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import project.addonis.models.Ide;
import project.addonis.repositories.interfaces.IdeRepository;

@Repository
public class IdeRepositoryImpl extends AbstractReadRepository<Ide> implements IdeRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public IdeRepositoryImpl(SessionFactory sessionFactory) {
        super(Ide.class, sessionFactory);
        this.sessionFactory=sessionFactory;
    }
}
