package project.addonis.repositories.interfaces;

import project.addonis.models.Tag;

public interface TagRepository extends BaseReadRepository<Tag>{
}
