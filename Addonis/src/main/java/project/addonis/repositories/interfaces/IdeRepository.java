package project.addonis.repositories.interfaces;

import project.addonis.models.Ide;

public interface IdeRepository extends BaseReadRepository<Ide>{
}
