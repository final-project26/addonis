package project.addonis.repositories.interfaces;

public interface BaseCUDRepository<T> {

    void create(T entity);

    void update(T entity);

    void delete(int id);
}
