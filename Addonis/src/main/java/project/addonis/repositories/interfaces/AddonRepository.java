package project.addonis.repositories.interfaces;

import project.addonis.models.Addon;
import project.addonis.models.User;

import java.util.List;
import java.util.Optional;

public interface AddonRepository extends BaseReadRepository<Addon>, BaseCUDRepository<Addon>{
    List<Addon> filter(Optional<String> addonName, Optional<String> ideName);

    List<Addon> sort(String criteria);

    String calculateRating(int id);

    void approve(int id);

}
