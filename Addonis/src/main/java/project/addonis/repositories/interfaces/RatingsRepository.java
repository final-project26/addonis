package project.addonis.repositories.interfaces;

import project.addonis.models.Rating;

public interface RatingsRepository extends BaseReadRepository<Rating>, BaseCUDRepository<Rating> {
}
