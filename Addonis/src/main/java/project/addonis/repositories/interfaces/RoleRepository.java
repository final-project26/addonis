package project.addonis.repositories.interfaces;

import project.addonis.models.Role;

public interface RoleRepository extends BaseReadRepository<Role> {
}
