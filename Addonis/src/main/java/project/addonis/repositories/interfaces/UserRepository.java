package project.addonis.repositories.interfaces;

import project.addonis.models.User;
import project.addonis.models.filterObjects.Ufp;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends BaseReadRepository<User>, BaseCUDRepository<User> {

      boolean isDuplicate(String name, String value, int id);

      List<User> filter(Ufp ufp);

      User getByUsername(String email);
      List<User> filter(Optional<String> usernameEmail, Optional<String> phoneNumber, Optional<String> sortParam);
}
