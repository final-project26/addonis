package project.addonis.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import project.addonis.exceptions.EntityNotFoundException;
import project.addonis.repositories.interfaces.BaseReadRepository;

import java.util.List;

import static java.lang.String.format;

public abstract class AbstractReadRepository<T> implements BaseReadRepository<T> {

    private final Class<T> clazz;
    private final SessionFactory sessionFactory;

    public AbstractReadRepository(Class<T> clazz, SessionFactory sessionFactory) {
        this.clazz = clazz;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public <V> T getByField(String name, V value) {
        final String query = format("from %s where %s = :value", clazz.getSimpleName(), name);
        final String notFoundErrorMessage = format("%s with %s %s not found", clazz.getSimpleName(), name, value);

        try (Session session = sessionFactory.openSession()) {
            return session.createQuery(query, clazz)
                    .setParameter("value", value)
                    .uniqueResultOptional()
                    .orElseThrow(() -> new EntityNotFoundException(notFoundErrorMessage));
        }
    }

    @Override
    public List<T> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery(format("from %s", clazz.getName(), clazz)).list();
        }
    }

    @Override
    public T getById(int id) {
        return getByField("id", id);
    }
}
