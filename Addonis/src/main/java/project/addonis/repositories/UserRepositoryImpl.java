package project.addonis.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import project.addonis.exceptions.EntityNotFoundException;
import project.addonis.models.User;
import project.addonis.models.filterObjects.Ufp;
import project.addonis.repositories.interfaces.UserRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;

@Repository
public class UserRepositoryImpl extends AbstractCUDRepository<User> implements UserRepository {

    public static final String INVALID_USER_INFORMATION = "User with such username, email or phone number does not exist";

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public boolean isDuplicate(String name, String value, int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(format("from User where %s = :value and id != :id", name), User.class);
            query.setParameter("value", value);
            query.setParameter("id", id);

            List<User> result = query.list();

            return result.size() != 0;
        }
    }

    @Override
    public List<User> filter(Ufp ufp) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" from User ");
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();

            if(!ufp.getUsername().equals("none")) {
                filters.add(" username like :username ");
                params.put("username", "%" + ufp.getUsername() + "%");
            }
            if(!ufp.getEmail().equals("none")){
                filters.add(" email like :email ");
                params.put("email", "%" + ufp.getEmail() + "%");
            }
            if(!ufp.getPhoneNumber().equals("none")){
                filters.add(" phone_number like :phoneNumber ");
                params.put("phoneNumber", "%" + ufp.getPhoneNumber() + "%");
            }

            if (!filters.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filters));
            }

            if (!ufp.getOrderBy().equals("none")) {
                String orderBy = generateOrderByString(ufp.getOrderBy());
                queryString.append(orderBy);
            }

            if (!ufp.getSort().equals("none") && !ufp.getOrderBy().equals("none")) {
                String sort = ufp.getSort();
                queryString.append(sort.equalsIgnoreCase("descending") ? " desc " : " asc ");
            }

            System.out.println(queryString);
            Query<User> query = session.createQuery(queryString.toString(), User.class);
            query.setProperties(params);

            return query.list();
        }
    }

    public List<User> filter(Optional<String> usernameEmail, Optional<String> phoneNumber, Optional<String> sortParam) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" from User ");
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();

            if (!usernameEmail.get().equals("")) {
                usernameEmail.ifPresent(value -> {
                    System.out.println("username:" + usernameEmail.get());
                    filters.add(" username like :username ");
                    params.put("username", value +"%");
                });
                usernameEmail.ifPresent(value -> {
                    filters.add(" email like :email ");
                    params.put("email", value+"%");
                });
            }
            if (!phoneNumber.get().equals("")) {
                phoneNumber.ifPresent(value -> {
                    filters.add(" phoneNumber like :phoneNumber ");
                    params.put("phoneNumber", value + "%");
                });
            }


            if (!filters.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filters));

            }
            if (!sortParam.get().equals("none")) {
                String sortString = generateQueryStringFromSortParam(sortParam);
                queryString.append(sortString);
            }
            Query<User> query = session.createQuery(queryString.toString(), User.class);
            System.out.println(queryString.toString());
            query.setProperties(params);

            return query.list();
        }
    }

    private String generateQueryStringFromSortParam(Optional<String> sortParam) {
        String paramStr = sortParam.get();
        switch (paramStr) {
            case "Username, ascending":
                return " order by username asc";
            case "Username, descending":
                return " order by username desc";
            case "Email, ascending":
                return " order by email asc";
            case "Email, descending":
                return " order by email desc";
        }

        return "Did not enter";

    }

    @Override
    public User getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);

            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "username", username);
            }

            return result.get(0);
        }
    }

    private String generateOrderByString(String orderBy) {
        switch (orderBy) {
            case "username":
                return " order by username ";
            case "email":
                return " order by email ";
        }

        return "";
    }
}

