package project.addonis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import project.addonis.exceptions.AuthenticationFailureException;
import project.addonis.exceptions.EntityNotFoundException;
import project.addonis.models.User;
import project.addonis.repositories.interfaces.UserRepository;
import project.addonis.services.interfaces.UserService;

import javax.servlet.http.HttpSession;


@Component
public class AuthenticationHelper {
    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String AUTHENTICATION_REQUIRED = "The requested resource requires authentication.";
    public static final String AUTHENTICATION_FAILURE_REST = "Invalid email address";
    public static final String AUTHENTICATION_FAILURE_MVC = "Wrong username or password";

    private final UserRepository userRepository;
    private final UserService userService;


    @Autowired
    public AuthenticationHelper(UserRepository userRepository, UserService userService) {
        this.userRepository = userRepository;
        this.userService = userService;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, AUTHENTICATION_REQUIRED);
        }


        try {
            String email = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            return userRepository.getByField("email", email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, AUTHENTICATION_FAILURE_REST);
        }

    }

    public User verifyAuthentication(String username, String password) {
        try {
            User user = userRepository.getByField("username", username);
            if (!user.getPassword().equals(password)) {
                throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MVC);
            }

            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MVC);
        }
    }

    public User tryGetUser(HttpSession session){
        String currentUser=(String) session.getAttribute("currentUser");
        if (currentUser == null) {
            throw new AuthenticationFailureException(AUTHENTICATION_REQUIRED);
        }

        return userService.getByUsername(currentUser);
    }


}
