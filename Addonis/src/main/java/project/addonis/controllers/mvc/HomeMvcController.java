package project.addonis.controllers.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import project.addonis.controllers.AuthenticationHelper;
import project.addonis.exceptions.AuthenticationFailureException;
import project.addonis.models.Addon;
import project.addonis.models.Ide;
import project.addonis.models.User;
import project.addonis.repositories.interfaces.AddonRepository;
import project.addonis.repositories.interfaces.IdeRepository;
import project.addonis.repositories.interfaces.UserRepository;
import project.addonis.services.interfaces.AddonService;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final IdeRepository ideRepository;
    private final AddonService addonService;
    private final AddonRepository addonRepository;
    private final UserRepository userRepository;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public HomeMvcController(IdeRepository ideRepository, AddonService addonService, AddonRepository addonRepository, UserRepository userRepository, AuthenticationHelper authenticationHelper) {
        this.ideRepository = ideRepository;
        this.addonService = addonService;
        this.addonRepository = addonRepository;
        this.userRepository = userRepository;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("ides")
    public List<Ide> populateIdes() {
        return ideRepository.getAll();
    }

    @ModelAttribute("addons")
    public List<Addon> populateAddons() {
        return addonService.getAll();
    }

    @ModelAttribute("newestAddons")
    public List<Addon> populateNewest() {
        return addonService.getNewest(5);
    }

    @ModelAttribute("mostDownloadedAddons")
    public List<Addon> populateMostDownloaded() {
        return addonService.getMostPopular(5);
    }

    @ModelAttribute("adminSelection")
    public List<Addon> populateAdminSelection() {
        return addonService.getFeatured(5);
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }
    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session, Model model){
        User user;
        try{
            user=authenticationHelper.tryGetUser(session);
            model.addAttribute("userId", user.getId());
            if(user.getRole().equals("admin")) return true;
        }catch (AuthenticationFailureException e){
            return false;
        }
        return false;
    }


    @GetMapping
    public String showHomePage(HttpSession session, Model model) {
        if (session.getAttribute("currentUser") != null) {
            String username = session.getAttribute("currentUser").toString();
            User user = userRepository.getByField("username", username);

            model.addAttribute("linkToPhoto", user.getPhoto());
        }
        return "index";
    }
}
