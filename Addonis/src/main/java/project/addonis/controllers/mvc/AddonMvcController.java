package project.addonis.controllers.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import project.addonis.models.*;
import project.addonis.models.dtos.EditAddonDto;
import project.addonis.models.dtos.RatingDto;
import org.springframework.web.bind.annotation.*;
import project.addonis.controllers.AuthenticationHelper;
import project.addonis.exceptions.AuthenticationFailureException;
import project.addonis.exceptions.EntityNotFoundException;
import project.addonis.exceptions.UnauthorizedOperationException;
import project.addonis.models.Addon;
import project.addonis.models.Ide;
import project.addonis.models.Tag;
import project.addonis.models.User;
import project.addonis.models.filterObjects.Afp;
import project.addonis.repositories.interfaces.IdeRepository;
import project.addonis.repositories.interfaces.RatingsRepository;
import project.addonis.repositories.interfaces.TagRepository;
import project.addonis.services.interfaces.AddonService;
import project.addonis.services.interfaces.UserService;
import project.addonis.services.mapper.AddonMapper;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URISyntaxException;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/addons")
public class AddonMvcController {

    private final AddonService addonService;
    private final UserService userService;
    private final TagRepository tagRepository;
    private final IdeRepository ideRepository;
    private final AuthenticationHelper authenticationHelper;
    private final AddonMapper addonMapper;
    private final RatingsRepository ratingsRepository;

    @Autowired
    public AddonMvcController(UserService userService, AddonMapper addonMapper, RatingsRepository ratingsRepository, AddonService addonService, TagRepository tagRepository, IdeRepository ideRepository, AuthenticationHelper authenticationHelper) {
        this.addonService = addonService;
        this.userService = userService;
        this.tagRepository = tagRepository;
        this.ideRepository = ideRepository;
        this.authenticationHelper = authenticationHelper;
        this.addonMapper = addonMapper;
        this.ratingsRepository = ratingsRepository;
    }

    @ModelAttribute("tags")
    public List<Tag> populateTags() {
        return tagRepository.getAll();
    }

    @ModelAttribute("ides")
    public List<String> populateIdes() {
        return ideRepository.getAll().stream().map(Ide::getName).collect(Collectors.toList());
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            model.addAttribute("userId", user.getId());
            if (user.getRole().equals("admin")) return true;
        } catch (AuthenticationFailureException e) {
            return false;
        }
        return false;
    }

    @ModelAttribute("isActive")
    public boolean populateIsActive(HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            return user.isActiveStatus();
        } catch (AuthenticationFailureException e) {
            return false;
        }
    }

    @ModelAttribute("idesObject")
    public List<Ide> populateIdesObject() {
        return ideRepository.getAll();
    }

    @ModelAttribute("sortParams")
    public List<String> populateSortParams() {
        return List.of("Name, Ascending", "Name, Descending", "Downloads, Ascending", "Downloads, Descending", "Upload Date, Ascending", "Upload Date, Descending", "Last Commit, Ascending", "Last Commit, Descending");
    }

    @GetMapping
    public String showAllAddons(Model model) {
        model.addAttribute("addons", addonService.getAll());
        model.addAttribute("addonFilterParams", new Afp());
        return "marketplace";
    }

    @GetMapping("/{id}")
    public String showAddon(@PathVariable int id, Model model) throws IOException {
        Addon addon = addonService.getById(id);
        model.addAttribute("addon", addon);
        long numberOfReviews = ratingsRepository.getAll().stream().map(Rating::getAddon).map(Addon::getId).filter(i -> i == id).count();
        model.addAttribute("numberOfReviews", numberOfReviews);
        double rating = Double.parseDouble(addonService.getRating(id).equals("") ? addonService.getRating(id).substring(8) : String.valueOf(0));
        model.addAttribute("rating", rating);
        model.addAttribute("newRating", new RatingDto());

        return "addon-profile";
    }


    @GetMapping("/new")
    public String showNewAddonPage(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("addon", new AddonDto());
        return "addon-create-form";
    }

    @PostMapping("")
    public String filter(@ModelAttribute("addonFilterParams") Afp afp, Model model) {
        List<Addon> addonList = null;
        if(afp.getName() != null) {
            addonList = addonService.filter(Optional.of(afp.getName()), Optional.empty());
        } else if(afp.getSort() != null) {
            addonList = addonService.sort(afp.getSort() != null ? afp.getSort() : "none");
        }
        if (addonList != null) {
            model.addAttribute("addons", addonList);
        }

        return "marketplace";
    }

    @GetMapping("/{id}/download")
    public ResponseEntity downloadAddon(@PathVariable int id) throws IOException, URISyntaxException {
        Addon addon = addonService.getById(id);
        return addonService.download(addon);
    }

    @GetMapping("/{id}/update")
    public String showEditAddonPage(@PathVariable int id, Model model, HttpSession session) throws IOException {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);

            Addon addon = addonService.getById(id);
            EditAddonDto addonDto = addonMapper.toDto(addon, user);

            model.addAttribute("originalAddon", addon);
            model.addAttribute("addonDto", addonDto);

            return "addon-update-form";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
        public String editAddon(@PathVariable int id, @Valid @ModelAttribute("addonDto") EditAddonDto editAddonDto,
                                BindingResult errors,
                                Model model,
                                HttpSession session) throws Exception {
            User user;
            try {
                user = authenticationHelper.tryGetUser(session);
            } catch (AuthenticationFailureException e) {
                return "redirect:/auth/login";
            }

            if (errors.hasErrors()) {
                System.out.println(errors);
                return "addon-update-form";
            }

            try {
               Addon addon = addonMapper.fromDto(id, Optional.of(editAddonDto.getDescription()), Optional.of(editAddonDto.getIcon()), Optional.of(editAddonDto.getTagSet()), user);
               addonService.update(addon);

                return "redirect:/addons/" + id;
            } catch (UnauthorizedOperationException e) {
                model.addAttribute("error", e.getMessage());
                return "access-denied";
            } catch (EntityNotFoundException e) {
                model.addAttribute("error", e.getMessage());
                return "not-found";
            }



        }

    @PostMapping("/{id}")
    public String rate(@PathVariable int id, @Valid @ModelAttribute("newRating") RatingDto rating, Model model, HttpSession session) throws IOException {
        User user = userService.getByUsername("nellyjekova");
        userService.rate(user, id, rating.getValue());

        return "redirect:/addons/" + id;
    }

    @PostMapping("/new")
    public String createAddon(@Valid @ModelAttribute("addon") AddonDto addonDto,
                              BindingResult errors,
                              Model model,
                              HttpSession session) throws Exception {

        if (errors.hasErrors()) {
            return "addon-create-form";
        }
        User user = userService.getByUsername("nellyjekova");
        Addon addon = addonMapper.fromDto(addonDto, user);
        addonService.create(addon);

        return "redirect:/addons";
    }


    @GetMapping("/pending")
    public String showPending(Model model, HttpSession session) {
        User user;
        try {
            authenticationHelper.tryGetUser(session);
            model.addAttribute("addons", addonService.getPending());
            return "pending-addons";
        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/pending/{id}")
    public String showPendingAddon(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            authenticationHelper.tryGetUser(session);
            model.addAttribute("addon", addonService.getPendingAddon(id).get(0));
            return "pending-addon";

        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("pending/{id}/delete")
    public String deletePendingAddon(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);

        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            addonService.delete(id, user);
            return "redirect:/users/pending";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException | UnsupportedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("pending/{id}/approve")
    public String approvePendingAddon(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);

        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            addonService.approve(id, user);
            return "redirect:/addons/pending";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException | UnsupportedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/pending/{id}/download")
    public ResponseEntity downloadPendingAddon(@PathVariable int id) throws IOException, URISyntaxException {
        Addon addon = addonService.getById(id);
        addon.setDownloads(addon.getDownloads() + 1);
        addonService.update(addon);
        return addonService.download(addon);
    }
}
