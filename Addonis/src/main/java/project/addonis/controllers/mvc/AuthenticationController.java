package project.addonis.controllers.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import project.addonis.controllers.AuthenticationHelper;
import project.addonis.exceptions.AuthenticationFailureException;
import project.addonis.exceptions.DuplicateEntityException;
import project.addonis.models.User;
import project.addonis.models.dtos.LoginDto;
import project.addonis.models.dtos.UserDto;
import project.addonis.services.interfaces.UserService;
import project.addonis.services.mapper.UserMapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {

    private final UserMapper userMapper;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public AuthenticationController(UserMapper userMapper, UserService userService, AuthenticationHelper authenticationHelper) {
        this.userMapper = userMapper;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto loginDto,
                              BindingResult result,
                              HttpSession session,
                              HttpServletRequest request) {

        if (result.hasErrors()) {
            return "login";
        }

        try {
            User user = authenticationHelper.verifyAuthentication(loginDto.getUsername(), loginDto.getPassword());
            session.setAttribute("currentUser", user.getUsername());
            session.setAttribute("currentUserEmail", user.getEmail());
            session.setAttribute("currentUserRole", user.getRole());
            session.setAttribute("currentUserStatus", user.isActiveStatus());

            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            result.rejectValue("username", "auth_error", e.getMessage());
            result.rejectValue("password", "auth_error", e.getMessage());

            return "login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new UserDto());
        return "register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") UserDto userDto,
                                 BindingResult result,
                                 HttpSession session) {
        if (result.hasErrors()) {
            return "register";
        }

        try {
            User user = userMapper.fromDtoToObject(userDto);
            userService.create(user);
            return "redirect:/auth/login";
        } catch (DuplicateEntityException e) {
            result.rejectValue("username", "username_error", e.getMessage());
            result.rejectValue("email", "email_error", e.getMessage());
            result.rejectValue("phoneNumber", "phoneNumber_error", e.getMessage());
            return "register";
        } catch (IllegalArgumentException e) {
            result.rejectValue("username", "username_error", e.getMessage());
            result.rejectValue("password", "password_error", e.getMessage());
            result.rejectValue("email", "email_error", e.getMessage());
            result.rejectValue("phoneNumber", "phoneNumber_error", e.getMessage());
            return "register";
        }

    }


    private Optional<String> getPreviousPageByRequest(HttpServletRequest request) {
        return Optional.ofNullable(request.getHeader("Referer")).map(requestUrl -> "redirect:" + requestUrl);
    }


}
