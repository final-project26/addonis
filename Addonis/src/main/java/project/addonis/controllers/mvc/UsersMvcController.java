package project.addonis.controllers.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import project.addonis.controllers.AuthenticationHelper;
import project.addonis.exceptions.AuthenticationFailureException;
import project.addonis.exceptions.EntityNotFoundException;
import project.addonis.exceptions.UnauthorizedOperationException;
import project.addonis.models.User;
import project.addonis.models.dtos.ChangePasswordDto;
import project.addonis.models.dtos.UsersUpdate;
import project.addonis.models.filterObjects.Ufp;
import project.addonis.services.interfaces.AddonService;
import project.addonis.services.interfaces.UserService;
import project.addonis.services.mapper.UserMapper;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/users")
public class UsersMvcController {

    private final UserService userService;
    private final AddonService addonService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper mapper;

    @Autowired
    public UsersMvcController(UserService userService, AddonService addonService, AuthenticationHelper authenticationHelper, UserMapper mapper) {
        this.userService = userService;
        this.addonService = addonService;
        this.authenticationHelper = authenticationHelper;
        this.mapper = mapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }
    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession session, Model model){
        User user;
        try{
            user=authenticationHelper.tryGetUser(session);
            model.addAttribute("userId", user.getId());
            if(user.getRole().equals("admin")) return true;
        }catch (AuthenticationFailureException e){
            return false;
        }
        return false;
    }

    @ModelAttribute("orderParams")
    public List<String> populateOrderParam(){
        return new ArrayList<>(List.of("username", "email"));
    }

    @ModelAttribute("sortParams")
    public List<String> populateSortParams() {
        return new ArrayList<>(List.of("ascending", "descending"));
    }

    @GetMapping()
    public String showAllUsers(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            model.addAttribute("users", userService.getAll(user));
            model.addAttribute("searchUsersDto", new Ufp());
            return "users-theme";
        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping()
    public String searchUsers(@ModelAttribute("searchUsersDto") Ufp searchUsersDto, Model model,
                              HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);

        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        List<User> userList = userService.filter(user, searchUsersDto);
        model.addAttribute("users", userList);
        return "users-theme";
    }

    @GetMapping("/{id}")
    public String showUser(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);

            User userToFind = userService.getById(user, id);
            System.out.println("user get photo:" +userToFind.getPhoto());
            model.addAttribute("user", userToFind);

            if (user.getId() == id) {
                return "user-theme";
            }
            else if (!user.getRole().equals("admin")) {

                return "redirect:/auth/login";
            }

            return "user-admin-theme";
        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }

    }

    @PostMapping("/{id}")
    public String showUser(@PathVariable int id, @ModelAttribute("user") User user,
                           BindingResult errors, Model model, HttpSession session) {
        User user1;
        try {
            user1 = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {

            return "redirect:/auth/login";
        }
        if (errors.hasErrors()) {
            if (user1.getRole().equals("admin"))
                return "user-admin-theme";
            else if (user1.getId() == id)
                return "user";
        }

        try {

            if (user.isActiveStatus()) userService.unblockUser(user1, id);
            else userService.blockUser(user1, id);

            return "redirect:/users";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/changePassword")
    public String changePassword(@PathVariable int id, Model model, HttpSession session) {

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (user.getId() != id) return "redirect:/auth/login";
            model.addAttribute("userToUpdatePassword", new ChangePasswordDto());
            return "change-password-theme";

        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.LOCKED, e.getMessage());
        }

    }


    @PostMapping("/{id}/changePassword")
    public String changePassword(@PathVariable int id, HttpSession session, @ModelAttribute("userToUpdatePassword") ChangePasswordDto changePasswordDto,
                                 BindingResult errors) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (errors.hasErrors()) {
            if (user.getId() != id) {
                return "redirect:/auth/login";
            }
            return "change-password-theme";
        }

        try {

            if (user.getId() != id) {
                return "redirect:/auth/login";
            }
            if (!user.getPassword().equals(changePasswordDto.getOldPass())) {
                errors.rejectValue("old password", "password_error");
                return "change-password";
            }
            if (!changePasswordDto.getNewPass().equals(changePasswordDto.getConfirmPass())) {
                errors.rejectValue("New password", "password_error");
                return "change-password";
            }
            user.setPassword(changePasswordDto.getNewPass());
            userService.update(user, user);
            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }



    @GetMapping("/{id}/update")
    public String showEditUserPage(@PathVariable int id,
                                   Model model,
                                   HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);

            User userToUpdate = userService.getById(user, id);
            if (user.getId() != id) return "redirect:/auth/login";

            UsersUpdate userDto = mapper.toDto(userToUpdate, user);
            model.addAttribute("userDto", userDto);
            return "user-update-form-theme";
            //}

        } catch (AuthenticationFailureException | UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("userDto") UsersUpdate userUpdateDto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {

            return "redirect:/auth/login";
        }
        if (errors.hasErrors()) {
            if (user.getId() != id) return "redirect:/auth/login";
            return "user-update-form-theme";
        }
        try {
            User userToUpdate = mapper.fromDtoToObject(id, userUpdateDto, user);

            userService.update(user, userToUpdate);
            return "redirect:/users/{id}";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY);
        }

    }



    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id, Model model, HttpSession session){
        User user;
        try{
            user=authenticationHelper.tryGetUser(session);
        }catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try{
            model.addAttribute("hasAddons", addonService.getAll().stream().filter(a -> a.getCreator().getEmail().equals(userService.getById(user, id).getEmail())).count() !=0);
            userService.delete(user, id);
            return "redirect:/";
        }catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException | UnsupportedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }
}
