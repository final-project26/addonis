package project.addonis.controllers.rest;


import com.cloudinary.Cloudinary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import project.addonis.controllers.AuthenticationHelper;
import project.addonis.exceptions.DuplicateEntityException;
import project.addonis.exceptions.EntityNotFoundException;
import project.addonis.exceptions.UnauthorizedOperationException;
import project.addonis.models.Addon;
import project.addonis.models.AddonDto;
import project.addonis.models.User;
import project.addonis.repositories.interfaces.UserRepository;
import project.addonis.services.interfaces.AddonService;
import project.addonis.services.mapper.AddonMapper;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

@RestController
@RequestMapping("api/addons")
public class AddonController {

    private final AuthenticationHelper authenticationHelper;
    private final AddonService service;
    private final AddonMapper mapper;
    private final UserRepository userRepository;
private final Cloudinary cloudinary;
    @Autowired
    public AddonController(AuthenticationHelper authenticationHelper, AddonService service, AddonMapper mapper, UserRepository userRepository, Cloudinary cloudinary) {
        this.authenticationHelper = authenticationHelper;
        this.service = service;
        this.mapper = mapper;
        this.userRepository = userRepository;
        this.cloudinary = cloudinary;
    }

    @GetMapping
    public List<Addon> getAll(){
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Addon getById(@PathVariable int id){
        //last_updated ---> check & update
        try{
            return service.getById(id);
        }catch(EntityNotFoundException | IOException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Addon create(@RequestHeader HttpHeaders headers, @RequestParam String name,
                        @RequestParam String description, @RequestParam int ideId, @RequestParam String linkToGitHub,
                        @RequestParam MultipartFile content,
                        @RequestParam MultipartFile icon, @RequestParam Set<Integer> tagSet) {


        try{
            User user=authenticationHelper.tryGetUser(headers);
            AddonDto addonDto=new AddonDto(name, description, ideId, icon, linkToGitHub, content, tagSet);
            Addon addon=mapper.fromDto(addonDto, user);
            service.create(addon);
            user.getAddons().add(addon);
            userRepository.update(user);
            return addon;
        }catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, e.getMessage()) ;
        }
    }


    @PutMapping("/{id}")
    public Addon update(@RequestHeader HttpHeaders headers,@PathVariable int id,
                        @RequestParam(required = false) Optional<String> description,
                        @RequestParam(required = false) Optional<MultipartFile> icon, @RequestParam(required = false) Optional<Set<Integer>> tagSet){
        try{
            User user = authenticationHelper.tryGetUser(headers);
            Addon addon=mapper.fromDto(id, description, icon, tagSet, user );
            service.update(addon);
            return addon;
        }catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }catch (UnauthorizedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers,@PathVariable int id){
        try{
            User user = authenticationHelper.tryGetUser(headers);
            service.delete(id, user);
        }catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (UnsupportedOperationException e){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<Addon> filter(@RequestParam(required = false)Optional<String> addonName,
                              @RequestParam(required = false)Optional<String> ideName){

            return service.filter(addonName, ideName);

    }

    @GetMapping("/sort")
    public List<Addon> sort(@RequestParam String criteria){
        try{
            return service.sort(criteria);
        }catch (IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/popular/{numberOfElements}")
    public List<Addon> getMostPopular(@PathVariable int numberOfElements){
        return service.getMostPopular(numberOfElements);

    }


    @GetMapping("/new/{numberOfElements}")
    public List<Addon> getNewest(@PathVariable int numberOfElements){
        return service.getNewest(numberOfElements);

    }

    //za featured kakvi da slozhim?

    @GetMapping("/featured/{numberOfElements}")
    public List<Addon> getFeatured(@PathVariable int numberOfElements){
        return service.getFeatured(numberOfElements);
    }

    //download

    @GetMapping("/{id}/download")
    public ResponseEntity downloadFile(@PathVariable int id) throws IOException, URISyntaxException {

        Addon addon= service.getById(id);
        return service.download(addon);

    }

    @GetMapping("/pending")
    public List<Addon> getPending(@RequestHeader HttpHeaders headers){

        User user;
        try{
            authenticationHelper.tryGetUser(headers);
            return service.getPending();

        }catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
