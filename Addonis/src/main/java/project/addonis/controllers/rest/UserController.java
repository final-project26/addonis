package project.addonis.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import project.addonis.controllers.AuthenticationHelper;
import project.addonis.exceptions.DuplicateEntityException;
import project.addonis.exceptions.EntityNotFoundException;
import project.addonis.exceptions.UnauthorizedOperationException;
import project.addonis.models.User;
import project.addonis.models.dtos.UserDto;
import project.addonis.models.filterObjects.Ufp;
import project.addonis.services.interfaces.UserService;
import project.addonis.services.mapper.UserMapper;

import javax.validation.Valid;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;

@RestController
@RequestMapping("/api/users")
public class UserController {

    public static final String SUCCESSFUL_UPLOAD = "Profile picture was successfully updated. File name: %s. Updated link: %s";
    public static final String SUCCESSFUL_DELETE = "User with id: %d was successfully deleted from the system";
    public static final String SUCCESSFUL_RATING = "You rated Addon %s with %d";

    public static final String BLOCKED_USER = "User with id %d is blocked and now has restricted access to the system";
    public static final String UNBLOCKED_USER = "User with id %d is unblocked and now has full access to the system";

    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UserService userService, UserMapper userMapper, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<User> getAll(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.getAll(user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }

    @GetMapping("/{id}")
    public User getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.getById(user, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @PostMapping
    public User create(@Valid @RequestBody UserDto userDto) {
        try {
            User user = userMapper.fromDtoToObject(userDto);
            userService.create(user);
            return user;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        }

    }

    @PutMapping("/{id}")
    public User update(@PathVariable int id, @Valid @RequestBody UserDto userDto, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            User toUpdate = userMapper.fromDtoToObject(id, userDto, user);

            userService.update(user, toUpdate);

            return toUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    @PutMapping("/block/{id}")
    public String blockUser(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            userService.blockUser(user, id);
            return format(BLOCKED_USER, id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/unblock/{id}")
    public String unblockUser(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            userService.unblockUser(user, id);
            return format(UNBLOCKED_USER, id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}/upload")
    public String uploadPhoto(@PathVariable int id, @RequestPart MultipartFile photo, @RequestHeader HttpHeaders headers) throws IOException {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            String uploadLink = userService.uploadPhoto(id, photo, user);
            return format(SUCCESSFUL_UPLOAD, photo.getOriginalFilename(), uploadLink);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("approve/addon/{id}")
    public String approveAddon(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.approveAddon(user, id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/rate/addon/{id}")
    public String rateAddon(@RequestPart String value, @PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            int rating = Integer.parseInt(value);
            String addonName = userService.rate(user, id, rating);
            return format(SUCCESSFUL_RATING, addonName, rating);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public String delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            userService.delete(user, id);
            return format(SUCCESSFUL_DELETE, id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED, e.getMessage());
        }

    }

    /*@GetMapping("/filter")
    public List<User> filter(@RequestHeader HttpHeaders headers,
                             @RequestParam (required = false) Optional<String> username,
                             @RequestParam (required = false) Optional<String> email,
                             @RequestParam (required = false) Optional<String> phoneNumber,
                             @RequestParam (required = false) Optional<String> sort,
                             @RequestParam (required = false) Optional<String> orderBy) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            Ufp ufp = new Ufp(username, email, phoneNumber, sort, orderBy);
            return userService.filter(user, ufp);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }*/

}
