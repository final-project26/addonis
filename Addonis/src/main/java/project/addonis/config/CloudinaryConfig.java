package project.addonis.config;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;



@Configuration
@PropertySource("classpath:application.properties")
public class CloudinaryConfig {

    private final String cloudName;
    private final String apiKey;
    private final String apiSecret;


    public CloudinaryConfig(Environment env) {
        cloudName = env.getProperty("cloudinary.cloud_name");
        apiKey = env.getProperty("cloudinary.api_key");
        apiSecret = env.getProperty("cloudinary.api_secret");
    }


    @Bean
    public Cloudinary cloudinary() {
        return new Cloudinary(ObjectUtils.asMap("cloud_name", cloudName,
                "api_key", apiKey,
                "api_secret", apiSecret,
                "secure", true));
    }
}
