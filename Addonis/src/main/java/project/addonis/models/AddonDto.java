package project.addonis.models;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

public class AddonDto {

    @NotNull
    @Size(min = 3, max = 30)
    private String name;

    @Size(max=150)
    private String description;

    @NotNull
    private int ide_id;

    @NotNull
    private String linkToGitHub;

    private MultipartFile icon;

    private MultipartFile content;

    private Set<Integer> tagSet;



    public AddonDto(String name, String description, int ide_id, MultipartFile icon, String linkToGitHub, MultipartFile content, Set<Integer> tagSet) {
        this.name = name;
        this.description = description;
        this.ide_id = ide_id;
        this.icon = icon;
        this.linkToGitHub = linkToGitHub;
        this.content = content;
        this.tagSet = tagSet;
    }





    public AddonDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIde_id() {
        return ide_id;
    }



    public void setIde_id(int ide_id) {
        this.ide_id = ide_id;
    }

    public String getLinkToGitHub() {
        return linkToGitHub;
    }

    public void setLinkToGitHub(String linkToGitHub) {
        this.linkToGitHub = linkToGitHub;
    }

    public MultipartFile getContent() {
        return content;
    }

    public void setContent(MultipartFile content) {
        this.content = content;
    }


    public MultipartFile getIcon() {
        return icon;
    }

    public void setIcon(MultipartFile icon) {
        this.icon = icon;
    }

    public Set<Integer> getTagSet() {
        return tagSet;
    }

    public void setTagSet(Set<Integer> tagSet) {
        this.tagSet = tagSet;
    }
}
