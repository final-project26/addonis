package project.addonis.models.filterObjects;

public class Afp {

    String name;

    String sort;

    public Afp() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

}
