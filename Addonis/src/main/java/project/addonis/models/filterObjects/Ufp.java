package project.addonis.models.filterObjects;

import java.util.Optional;

//User filter params class for filter method
public class Ufp {

    String username;


    String email;

    String phoneNumber;

    String sort;

    String orderBy;

    public Ufp(){

    }

    public Ufp(String username, String email, String phoneNumber, String sort, String orderBy) {
        this.username = username;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.sort = sort;
        this.orderBy = orderBy;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }
}
