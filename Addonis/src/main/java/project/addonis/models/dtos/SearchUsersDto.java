package project.addonis.models.dtos;

public class SearchUsersDto {
    String usernameEmail;
    String phoneNumber;
    String sortParam;



    public SearchUsersDto() {
    }

    public SearchUsersDto(String usernameEmail, String phoneNumber, String sortParam) {
        this.usernameEmail = usernameEmail;
        this.phoneNumber = phoneNumber;
        this.sortParam=sortParam;
    }

    public String getUsernameEmail() {
        return usernameEmail;
    }

    public void setUsernameEmail(String usernameEmail) {
        this.usernameEmail = usernameEmail;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public String getSortParam() {
        return sortParam;
    }

    public void setSortParam(String sortParam) {
        this.sortParam = sortParam;
    }
}
