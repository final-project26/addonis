package project.addonis.models.dtos;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class ChangePasswordDto {

    private String oldPass;

    @Size(min = 8, message = "Password must have at least 8 symbols.")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$", message = "password must be at least 8 characters long " +
            "with 1 uppercase, 1 lowercase 1, special character & 1 numeric character.")
    private String newPass;

    private String confirmPass;

    public ChangePasswordDto() {
    }

    public String getOldPass() {
        return oldPass;
    }

    public void setOldPass(String oldPass) {
        this.oldPass = oldPass;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

    public String getConfirmPass() {
        return confirmPass;
    }

    public void setConfirmPass(String confirmPass) {
        this.confirmPass = confirmPass;
    }
}
