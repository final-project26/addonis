package project.addonis.models.dtos;

import org.springframework.web.multipart.MultipartFile;
import project.addonis.models.Role;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Optional;

public class UsersUpdate {


    @Email
    private String email;

    @Size(min=10, max=10, message = "Phone number must have ten digits")
    private String phoneNumber;

    private Optional<MultipartFile> photo;

    private String photoLink;

    public String getPhotoLink() {
        return photoLink;
    }

    public void setPhotoLink(String photoLink) {
        this.photoLink = photoLink;
    }

    public UsersUpdate() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Optional<MultipartFile> getPhoto() {
        return photo;
    }

    public void setPhoto(Optional<MultipartFile> photo) {
        this.photo = photo;
    }
}
