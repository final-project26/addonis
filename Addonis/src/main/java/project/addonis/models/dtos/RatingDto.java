package project.addonis.models.dtos;

import org.hibernate.validator.constraints.Range;


public class RatingDto {

   @Range(min = 1, max = 5)
    int value;

    public RatingDto() {
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
