package project.addonis;

import project.addonis.models.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class Helpers {
    public static User createMockEmployee(){
        var mockEmployee=new User();
        mockEmployee.setId(1);
        mockEmployee.setUsername("mockEmployee");
        mockEmployee.setEmail("email@mock.com");
        mockEmployee.setPassword("mockPassword123!");
        mockEmployee.setPhoneNumber("0123456789");
        mockEmployee.setPhoto("mockPhoto");
        mockEmployee.setActiveStatus(true);
        mockEmployee.setRole(createMockRoleAdmin());
        mockEmployee.setAddons(new HashSet<>());

        return mockEmployee;

    }

    public static User createMockUser(){
        var mockUser=new User();
        mockUser.setId(2);
        mockUser.setUsername("mockUser");
        mockUser.setEmail("email@mock.com");
        mockUser.setPassword("mockPassword123!");
        mockUser.setPhoneNumber("0123456788");
        mockUser.setPhoto("mockPhoto");
        mockUser.setActiveStatus(true);
        mockUser.setRole(createMockRoleUser());
        mockUser.setAddons(new HashSet<>());

        return mockUser;

    }

    public static  Role createMockRoleUser(){

        var mockRole=new Role();
        mockRole.setId(1);
        mockRole.setType("user");
        return mockRole;
    }
    public static Role createMockRoleAdmin(){

        var mockRole=new Role();
        mockRole.setId(2);
        mockRole.setType("admin");

        return mockRole;
    }

    public static Set<Addon> createMockSetAddon(){

        Set<Addon> mockSetAddon=new HashSet<>();
        mockSetAddon.add(createMockAddonApproved());

        return  mockSetAddon;
    }

    public static Addon createMockAddonApproved(){
        var mockAddon=new Addon();
        mockAddon.setId(1);
        mockAddon.setStatus("approved");
        mockAddon.setDownloads(1);
        mockAddon.setName("mockAddon");
        mockAddon.setOrigin("mockOrigin");
        mockAddon.setUpdateDate(LocalDate.now());
        mockAddon.setLastCommit(LocalDateTime.now());
        mockAddon.setDescription("mockDescription");
        mockAddon.setIde(createMockIde());
        mockAddon.setCreator(new User());
        mockAddon.setOpenIssues(1);
        mockAddon.setPullRequests(1);
        mockAddon.setContent("mockContent");
        mockAddon.setIcon("mockIcon");
        mockAddon.setTagSet(createMockTagSet());

        return mockAddon;

    }

    public static Ide createMockIde(){
        var mockIde=new Ide();
        mockIde.setId(1);
        mockIde.setName("mockIde");

        return mockIde;
    }

    public static Set<Tag> createMockTagSet(){
        Set<Tag> mockTagSet=new HashSet<>();
        mockTagSet.add(createMockTag());

        return mockTagSet;
    }

    public static Tag createMockTag(){
        var mockTag=new Tag();
        mockTag.setId(1);
        mockTag.setName("mockTagName");

        return mockTag;
    }

    public static Rating createMockRating(){
        var mockRating=new Rating();
        mockRating.setRating(1);
        mockRating.setId(1);
        mockRating.setUser(createMockEmployee());
        mockRating.setAddon(createMockAddonApproved());

        return mockRating;
    }
}
