package project.addonis.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import project.addonis.Helpers;
import project.addonis.exceptions.EntityNotFoundException;
import project.addonis.exceptions.UnauthorizedOperationException;
import project.addonis.models.User;
import project.addonis.repositories.interfaces.UserRepository;
import project.addonis.services.interfaces.UserService;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @Mock
    UserRepository mockRepository;

    @InjectMocks
    UserServiceImpl service;

    @Test
    public void getAll_should_callRepository_when_initiatorIsAdmin() {
        var mockEmployee = Helpers.createMockEmployee();

        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll(mockEmployee);

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getAll_should_throwException_when_initiatorIsNotAdmin() {
        var mockCustomer = Helpers.createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.getAll(mockCustomer));
    }

    @Test
    public void getById_should_returnUser_when_idExists() {
        var mockEmployee = Helpers.createMockEmployee();
        //Arrange
        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockEmployee);

        //Act
        User result = service.getById(mockEmployee,1);

        //Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("email@mock.com", result.getEmail());

    }

    @Test
    public void getById_should_throw_when_userIsNotAuthorized() {
        var mockCustomer = Helpers.createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.getById( mockCustomer,1));

    }



}
